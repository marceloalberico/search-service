const dotenv = require('dotenv');
dotenv.config()

module.exports = {
    appUrl: process.env.APP_URL,
    elasticSearchConfig: process.env.ELASTIC_SEARCH_CONFIG
} 

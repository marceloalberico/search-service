const { Client } = require('@elastic/elasticsearch')
const { elasticSearchConfig } = require('../config/app')

module.exports = () => {
    try {
        console.log(elasticSearchConfig)
        return new Client(JSON.parse(elasticSearchConfig))
    } catch (ex) {
        // TODO: Graylog
    }    
}

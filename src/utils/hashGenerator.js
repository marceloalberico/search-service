const crypto = require('crypto')

module.exports = () => {
    const buffer = crypto.randomBytes(16)

    return buffer.toString('hex')
}

module.exports = {
    create: 'create',
    update: 'update',
    delete: 'delete',
    search: {
        aggregation: 'aggregation'
    },
    status: {
        success: 200,
        invalidRequest: 400,
        serverError: 500
    },
    validate: {
        success: 1,
        failed: 0
    },
    message: {
        database_not_available: "Database not available"
    }
}
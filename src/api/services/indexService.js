const indexConstants = require('../constants/indexConstants')
const validateRepository = require('../repositories/validateRepository')
const indexRepository = require('../repositories/indexRepository')

module.exports = {
	async search(req) {
		try {
			const { pathParameters, body } = req
			const validate = await validateRepository.search(body)
	
			if (validate.status != indexConstants.validate.success) {
				return {
					statusCode: indexConstants.status.invalidRequest,
					body: JSON.stringify(validate.body)
				}
			}

            const response = await indexRepository.search(pathParameters.index, body)
		
			return {
				statusCode: indexConstants.status.success,
				body: response
			}
		} catch (e) {
	
		}
	},	
	async create(req) {  
		try {
			const { pathParameters, body } = req
			const validate = await validateRepository.create(body)
	
			if (validate.status != indexConstants.validate.success) {
				return {
					statusCode: indexConstants.status.invalidRequest,
					body: JSON.stringify(validate.body)
				}
			}
			
			const response = await indexRepository.create(pathParameters.index, pathParameters.key, body)
		
			return {
				statusCode: indexConstants.status.success,
				body: response
			}
		} catch (e) {
	
		}
	},
	async update(req) {
		try {
			const { pathParameters, body } = req
			const validate = await validateRepository.update(body)
	
			if (validate.status != indexConstants.validate.success) {
				return {
					statusCode: indexConstants.status.invalidRequest,
					body: JSON.stringify(validate.body)
				}
			}
			
			const response = await indexRepository.update(pathParameters.index, pathParameters.key, body)
		
			return {
				statusCode: indexConstants.status.success,
				body: response
			}
		} catch (e) {
	
		}
	},
	async delete(req) {
		try {
			const { pathParameters, body } = req        
			const response = await indexRepository.delete(pathParameters.index, pathParameters.key, body)
		
			return {
				statusCode: indexConstants.status.success,
				body: response
			}
		} catch (e) {
	
		}
	},
}
const { Client } = require('@elastic/elasticsearch')
const config = require('../../config/app')

module.exports = {
	async conn() {
        try {
            return new Client(JSON.parse(config.elasticSearchConfig))
        } catch (e) {
        }
    },
}
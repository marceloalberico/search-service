const indexService = require('../services/indexService')
const indexConstants = require('../constants/indexConstants')

module.exports = {
	async search(req) {  
		try {
			response = await indexService.search(req)

			return {
				statusCode: response.statusCode,
				body: JSON.stringify(response.body)
			}
		} catch (e) {
			return {
				statusCode: indexConstants.status.serverError,
				body: JSON.stringify({
					"message": e
				})
			}
		}
	},
	async create(req) {  
		try {
			response = await indexService.create(req)

			return {
				statusCode: response.statusCode,
				body: JSON.stringify(response.body)
			}
		} catch (e) {
			return {
				statusCode: indexConstants.status.serverError,
				body: JSON.stringify({
					"message": e
				})
			}
		}
	},
	async update(req) {  
		try {
			response = await indexService.update(req)

			return {
				statusCode: response.statusCode,
				body: JSON.stringify(response.body)
			}
		} catch (e) {
			return {
				statusCode: indexConstants.status.serverError,
				body: JSON.stringify({
					"message": e
				})
			}
		}
	},
	async delete(req) {  
		try {
			response = await indexService.delete(req)

			return {
				statusCode: response.statusCode,
				body: JSON.stringify(response.body)
			}
		} catch (e) {
			return {
				statusCode: indexConstants.status.serverError,
				body: JSON.stringify({
					"message": e
				})
			}
		}
	}
}
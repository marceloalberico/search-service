const databaseService = require('../services/databaseService')
const indexConstants = require('../constants/indexConstants')

module.exports = {
    async search(index,query) {
        try {
            const client = await databaseService.conn()

            if (!client) {
                throw(indexConstants.message.database_not_available)
            }

            const data = JSON.parse(query)
            const body = this.command(data)
            const response = await client.search({
                index: index,
                body: body
            })
            
            return this.handler(data.type, response)
        } catch (e) {
            console.log(e)
        }
    },
	async create(index, id, query) {
        try {
            const client = await databaseService.conn()

            if (!client) {
                throw(indexConstants.message.database_not_available)
            }

            const response = await client.index({
                index: index,
                id: id,
                body: query
            })

            return this.handler(indexConstants.create, response)
        } catch (e) {
            console.log(e)
        }
    },
    async update(index, id, query) {
        try {
            const client = await databaseService.conn()

            if (!client) {
                throw(indexConstants.message.database_not_available)
            }

            const response = await client.update({
                index: index,
                id: id,
                body: query
            })

            return this.handler(indexConstants.update, response)
        } catch (e) {
            console.log(e)
        }
    },
    async delete(index, id) {
        try {
            const client = await databaseService.conn()

            if (!client) {
                throw(indexConstants.message.database_not_available)
            }

            const response = await client.delete({
                index: index,
                id: id
            })

            return this.handler(indexConstants.delete, response)
        } catch (e) {
            console.log(e)
        }
    },
    command(data) {
        if (data.type == indexConstants.search.aggregation) {
            return {
                "_source": [data.key], 
                    "size": 0,
                "query": data.query,
                "aggs":{
                    "key": {
                        "terms": {
                            "field": data.key
                        }
                    }
                }
            }
        }       
    },
    handler(type, data) {
        switch (type) {
            case indexConstants.search.aggregation: 
                return data.body.aggregations.key.buckets
            case indexConstants.create: 
                return data.body.result
            case indexConstants.update: 
                return data.body.result
            case indexConstants.delete: 
                return data.body.result
        }
    }
}
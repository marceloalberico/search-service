const Yup = require('yup')
const indexConstants = require('../constants/indexConstants')

module.exports = {
	async search(body) {
        try {
            const schema = Yup.object().shape({
                type: Yup.string().required(),
                key: Yup.string().required(),
                query:  Yup.string().required()
            })

            await schema.validate(body, { abortEarly: false })

            return {
                status: indexConstants.validate.success
            }
        } catch (e) {
            return {
                status: indexConstants.validate.failed,
                message: e.inner.map(e => e.message)
            }
        }
    },
    async create(body) {
        try {
            const schema = Yup.object().shape({
                company_id: Yup.string().required(),
                contact_id: Yup.string().required()
            })

            await schema.validate(body, { abortEarly: false })

            return {
                status: indexConstants.validate.success
            }
        } catch (e) {
            return {
                status: indexConstants.validate.failed,
                message: e.inner.map(e => e.message)
            }
        }
    },
    async update(body) {
        try {
            const schema = Yup.object().shape({
                doc: Yup.string().required()
            })

            await schema.validate(body, { abortEarly: false })

            return {
                status: indexConstants.validate.success
            }
        } catch (e) {
            return {
                status: indexConstants.validate.failed,
                message: e.inner.map(e => e.message)
            }
        }
    },
}
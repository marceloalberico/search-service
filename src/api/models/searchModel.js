module.exports = (sequelize, DataTypes) => {
    const Search = sequelize.define(
      'Search',
      {
        type: DataTypes.STRING,
        key: DataTypes.STRING,
        query: DataTypes.STRING
      }
    )
  
    return Search
  }